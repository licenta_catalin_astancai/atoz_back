package com.atoz.atoz.entities;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;
@Entity
public class Customer extends Users implements Serializable {


    private static final long serialVersionUID = 1L;



    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;


    @Column(name = "nume", nullable = false)
    private String nume;

    @Column(name = "telefon", nullable = false)
    private String telefon;

    @Column(name = "adresa", nullable = false)
    private String adresa;


    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER)
    @JsonBackReference
    private List<Order> listOfOrder;


    public Customer(){

    }

    public Customer(UUID id, String nume, String telefon, String adresa) {
        this.id = id;
        this.nume = nume;
        this.telefon = telefon;
        this.adresa = adresa;
    }

    public Customer(String username, String password,String role, String nume, String telefon, String adresa) {
        super(username, password,role);
        this.nume = nume;
        this.telefon = telefon;
        this.adresa = adresa;
    }



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }




}
