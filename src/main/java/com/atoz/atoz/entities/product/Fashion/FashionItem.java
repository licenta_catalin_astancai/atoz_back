package com.atoz.atoz.entities.product.Fashion;

import com.atoz.atoz.entities.product.Product;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class FashionItem extends Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;


    @Column(name = "gender")
    private String gender;

    @Column(name = "type")
    private String type;

    @Column(name = "brand")
    private String brand;

    @Column(name = "size")
    private String size;

    @Column(name = "color")
    private String color;

    public FashionItem(String name, float price, String productImage,int stock,String category,String gender, String type, String brand, String size, String color) {
        super(name, price, productImage,stock,category);
        this.gender = gender;
        this.type = type;
        this.brand = brand;
        this.size = size;
        this.color = color;
    }

    public FashionItem() {
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
