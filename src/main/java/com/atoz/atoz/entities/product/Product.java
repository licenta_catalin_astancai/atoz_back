package com.atoz.atoz.entities.product;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price", nullable = false)
    private float price;

    @Column(name = "image",nullable = true,length = 10000000)
    private String productImage;

    @Column(name = "stock",nullable = true, length = 64)
    private int stock;

    @Column(name = "category",nullable = true, length = 64)
    private String category;

    public Product(int id, String name, float price, String productImage,int stock,String category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.productImage = productImage;
        this.stock = stock;
        this.category=category;
    }

    public Product(String name, float price, String productImage,int stock,String category) {
        this.name = name;
        this.price = price;
        this.productImage = productImage;
        this.stock=stock;
        this.category=category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Product() {
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}

