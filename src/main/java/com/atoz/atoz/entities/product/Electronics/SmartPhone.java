package com.atoz.atoz.entities.product.Electronics;

import com.atoz.atoz.entities.product.Product;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class SmartPhone extends Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "dispsize")
    private String dispsize;

    @Column(name = "resolution")
    private String resolution;

    @Column(name = "maincamera")
    private String maincamera;

    @Column(name = "frontcamera")
    private String frontcamera;

    @Column(name = "cpu")
    private String cpu;

    @Column(name = "memory")
    private String memory;

    @Column(name = "battery")
    private String battery;

    @Column(name = "description")
    private String description;

    public SmartPhone(String name, float price, String productImage,int stock,String category,String brand, String model, String dispsize, String resolution, String maincamera, String frontcamera, String cpu, String memory, String battery, String description) {
        super(name, price, productImage,stock,category);
        this.brand=brand;
        this.model = model;
        this.dispsize = dispsize;
        this.resolution = resolution;
        this.maincamera = maincamera;
        this.frontcamera = frontcamera;
        this.cpu = cpu;
        this.memory = memory;
        this.battery = battery;
        this.description = description;
    }

    public SmartPhone() {

    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDispsize() {
        return dispsize;
    }

    public void setDispsize(String dispsize) {
        this.dispsize = dispsize;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String rezolutie) {
        this.resolution = rezolutie;
    }

    public String getMaincamera() {
        return maincamera;
    }

    public void setMaincamera(String maincamera) {
        this.maincamera = maincamera;
    }

    public String getFrontcamera() {
        return frontcamera;
    }

    public void setFrontcamera(String frontcamera) {
        this.frontcamera = frontcamera;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
