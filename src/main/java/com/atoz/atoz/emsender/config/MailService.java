package com.atoz.atoz.emsender.config;

import com.atoz.atoz.emsender.bean.Mail;

public interface MailService {
    public void sendEmail(Mail email);
}
