package com.atoz.atoz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.validation.annotation.Validated;

import java.util.TimeZone;

@SpringBootApplication
@Validated
public class AtozApplication {

    public static void main(String[] args) {
        //TimeZone.setDefault(TimeZone.getTimeZone("UTC"));


        SpringApplication.run(AtozApplication.class, args);
    }

}
