package com.atoz.atoz.services;

import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.dtos.builders.ElectronicsBuilder.SmartPhoneBuilder;
import com.atoz.atoz.dtos.builders.FashionBuilder.FashionBuilder;
import com.atoz.atoz.entities.product.Electronics.SmartPhone;
import com.atoz.atoz.entities.product.Fashion.FashionItem;
import com.atoz.atoz.repositories.FashionItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FashionItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FashionItemService.class);
    private final FashionItemRepository fashionItemRepository;

    public FashionItemService(FashionItemRepository fashionItemRepository) {
        this.fashionItemRepository = fashionItemRepository;
    }

    public List<FashionItemDTO> findFashionItem() {
        List<FashionItem> fashionItemList = fashionItemRepository.findAll();
        return fashionItemList.stream()
                .map(FashionBuilder::toFashionItemDTO)
                .collect(Collectors.toList());
    }

    public FashionItemDTO findFashionItemById(int id){
        Optional<FashionItem> fashionItem = fashionItemRepository.findById(id);
        return FashionBuilder.toFashionItemDTO(fashionItem.get());
    }

    public int insert(FashionItemDTO fashionItemDTO){
        FashionItem fashionItem = FashionBuilder.toEntity(fashionItemDTO);
        fashionItem = fashionItemRepository.save(fashionItem);
        LOGGER.debug("fashionItem with id {} was inserted in db", fashionItem.getId());
        return fashionItem.getId();
    }

    public int delete(int id){
        Optional<FashionItem> optionalFashionItem = fashionItemRepository.findById(id);
        FashionItem fashionItem = optionalFashionItem.get();


        fashionItemRepository.deleteById(id);
        return id;
    }

    public int update (int id,FashionItemDTO fashionItemDTO){
        Optional<FashionItem> optionalFashionItem = fashionItemRepository.findById(id);

        optionalFashionItem.get().setBrand(fashionItemDTO.getBrand());
        optionalFashionItem.get().setColor(fashionItemDTO.getColor());
        optionalFashionItem.get().setName(fashionItemDTO.getName());
        optionalFashionItem.get().setPrice(fashionItemDTO.getPrice());
        optionalFashionItem.get().setProductImage(fashionItemDTO.getProductImage());
        optionalFashionItem.get().setSize(fashionItemDTO.getSize());
        optionalFashionItem.get().setColor(fashionItemDTO.getColor());
        optionalFashionItem.get().setCategory(fashionItemDTO.getCategory());
        optionalFashionItem.get().setGender(fashionItemDTO.getGender());
        optionalFashionItem.get().setType(fashionItemDTO.getType());
        System.out.println(  optionalFashionItem.get().toString());
        fashionItemRepository.save( optionalFashionItem.get());

        return optionalFashionItem.get().getId();
    }
}
