package com.atoz.atoz.services;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.Order.OrderDTO;
import com.atoz.atoz.dtos.builders.CustomerBuilder;
import com.atoz.atoz.dtos.builders.OrderBuilder.OrderBuilder;
import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.entities.Order;
import com.atoz.atoz.repositories.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<OrderDTO> findOrder() {
        List<Order> orderList = orderRepository.findAll();

        return orderList.stream()
                .map(OrderBuilder::toOrderDTO)
                .collect(Collectors.toList());
    }
    public int insert(OrderDTO orderDTO){
        Order order = OrderBuilder.toEntity(orderDTO);

        order = orderRepository.save(order);
        LOGGER.debug("order with id {} was inserted in db", order.getId());
        return order.getId();
    }


    public List<Integer>  processOrderMonth(){
        List<Order> orderList = orderRepository.findAll();
        List<Integer> monthIn=new ArrayList<Integer>();
        int ianuarie=0,februarie=0,martie=0,aprilie=0,mai=0,iunie=0,iulie=0,august=0,septembrie=0,octombrie=0,noiembrie=0,decembrie=0;

        int luna;
        for(int i=0; i<orderList.size();i++){
            luna=orderList.get(i).getOrderDate().getMonth()+1;
            switch (luna){
                case 1:ianuarie+=1;
                break;
                case 2:februarie+=1;
                break;
                case 3:martie+=1;
                break;
                case 4:aprilie+=1;
                break;
                case 5:mai+=1;
                break;
                case 6:iunie+=1;
                break;
                case 7:iulie+=1;
                break;
                case 8:august+=1;
                break;
                case 9:septembrie+=1;
                break;
                case 10:octombrie+=1;
                break;
                case 11:noiembrie+=1;
                break;
                case 12:decembrie+=1;
                break;
            }
        }
        monthIn.add(ianuarie);
        monthIn.add(februarie);
        monthIn.add(martie);
        monthIn.add(aprilie);
        monthIn.add(mai);
        monthIn.add(iunie);
        monthIn.add(iulie);
        monthIn.add(august);
        monthIn.add(septembrie);
        monthIn.add(octombrie);
        monthIn.add(noiembrie);
        monthIn.add(decembrie);
        System.out.println("okokokok");
        System.out.println(monthIn.toString());
        return monthIn;
    }
}
