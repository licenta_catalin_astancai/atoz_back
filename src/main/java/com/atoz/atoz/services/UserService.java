package com.atoz.atoz.services;

import com.atoz.atoz.authenticator.repo.UserRepository;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.dtos.UsersDTO;
import com.atoz.atoz.dtos.builders.FashionBuilder.FashionBuilder;
import com.atoz.atoz.dtos.builders.UserBuilder;
import com.atoz.atoz.entities.Users;
import com.atoz.atoz.entities.product.Fashion.FashionItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UsersDTO> finUsers() {
        List<Users> usersList = userRepository.findAll();
        return usersList.stream()
                .map(UserBuilder::usersDTO)
                .collect(Collectors.toList());
    }

    public UsersDTO findUserById(UUID id){
        Optional<Users> user = userRepository.findById(id);
        return UserBuilder.usersDTO(user.get());
    }
}
