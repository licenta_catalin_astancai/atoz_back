package com.atoz.atoz.services;

import com.atoz.atoz.dtos.Order.OrderDTO;
import com.atoz.atoz.dtos.Order.OrderDetailsDTO;
import com.atoz.atoz.dtos.Order.OrdersDetailsDTO;
import com.atoz.atoz.dtos.builders.OrderBuilder.OrderBuilder;
import com.atoz.atoz.dtos.builders.OrderBuilder.OrderDetailsBuilder;
import com.atoz.atoz.emsender.bean.Mail;

import com.atoz.atoz.emsender.services.MailServiceImplementation;
import com.atoz.atoz.entities.Order;
import com.atoz.atoz.entities.OrderDetails;
import com.atoz.atoz.entities.product.Product;
import com.atoz.atoz.repositories.OrderDetailsRepository;
import com.atoz.atoz.repositories.OrderRepository;
import com.atoz.atoz.repositories.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDetailsService.class);
    private final OrderDetailsRepository orderDetailsRepository;
    private final OrderRepository orderRepository;
    private final ProductsRepository productsRepository;
    private final CustomerService customerService;
    private final ProductService productService;
    private final MailServiceImplementation mailServiceImplementation;

    @Autowired
    public OrderDetailsService(OrderDetailsRepository orderDetailsRepository, OrderRepository orderRepository, ProductsRepository productsRepository, CustomerService customerService, ProductService productService, MailServiceImplementation mailServiceImplementation) {
        this.orderDetailsRepository = orderDetailsRepository;
        this.orderRepository = orderRepository;
        this.productsRepository = productsRepository;
        this.customerService = customerService;
        this.productService = productService;
        this.mailServiceImplementation = mailServiceImplementation;
    }

    @Autowired
    public List<OrderDetailsDTO> findOrdersDetails() {
        List<OrderDetails> orderDetailsList = orderDetailsRepository.findAll();

        return orderDetailsList.stream()
                .map(OrderDetailsBuilder::toOrderDetailsDTO)
                .collect(Collectors.toList());
    }

    public int insert(OrdersDetailsDTO ordersDetailsDTO) {

        int id = ordersDetailsDTO.getId();
        OrderDTO orderDTO = new OrderDTO(ordersDetailsDTO.getCustomerID(), ordersDetailsDTO.getDate());
        Order preOrder = OrderBuilder.toEntity(orderDTO);
        orderRepository.save(preOrder);
        int sum = 0;
        Optional<Order> o = orderRepository.findById(preOrder.getId());
        String text;
        StringBuilder builder=new StringBuilder();
        int dif=0;
        for (int i = 0; i < ordersDetailsDTO.getProductList().size(); i++) {
            Optional<Product> product = productsRepository.findById(ordersDetailsDTO.getProductList().get(i).getId());

            float unitPrice = ordersDetailsDTO.getUnitPriceList().get(i);
            int quantity = ordersDetailsDTO.getQuantity().get(i);
            int stock=product.get().getStock();

            dif=stock-quantity;
            productService.updateStock(ordersDetailsDTO.getProductList().get(i).getId(),dif);
            sum += (unitPrice * quantity);
            text ="\n"+product.get().getName() + " pret: " + unitPrice + " " +" cantitate: "+quantity+" ";
            builder.append(text);
            OrderDetailsDTO aux = new OrderDetailsDTO(id, o.get(), product.get(), unitPrice, quantity);
            orderDetailsRepository.save(OrderDetailsBuilder.toEntity(aux));
        }
        String userEmail = customerService.findCustomerById(ordersDetailsDTO.getCustomerID().getId()).getUsername();

        Mail mail = new Mail();
        mail.setMailFrom("atozecommerce21@gmail.com");
        System.out.println(userEmail);
        mail.setMailTo(userEmail);
        mail.setMailSubject("Comanda ta a fost inregistrata de AtoZ");
        mail.setMailContent("Buna ziua, \n\n" + customerService.findCustomerById(ordersDetailsDTO.getCustomerID().getId()).getNume() +" avand numarul de telefon: "+ customerService.findCustomerById(ordersDetailsDTO.getCustomerID().getId()).getTelefon()+". Faptul ca ai comandat de la noi inseamna foarte mult pentru compania noastra. \n" +
                "\nLista produselor comandate este:"
                + builder.toString() + "\n\nTotalul de plata este de: " + sum + "Lei");

        mailServiceImplementation.sendEmail(mail);


        return preOrder.getId();
    }

    public List<Float> turnOverPerMonth(){
        int luna;
        List<Float> monthIn=new ArrayList<Float>();
        float ianuarie=0,februarie=0,martie=0,aprilie=0,mai=0,iunie=0,iulie=0,august=0,septembrie=0,octombrie=0,noiembrie=0,decembrie=0;
        List<OrderDetails> orderDetailsList = orderDetailsRepository.findAll();
        for(int i=0; i<orderDetailsList.size();i++){
            float unitPrice = orderDetailsList.get(i).getUnitPrice();
            System.out.println(unitPrice);
            int quantity = orderDetailsList.get(i).getQuantity();
            System.out.println(quantity);
            luna=orderDetailsList.get(i).getOrder().getOrderDate().getMonth()+1;
            switch (luna){
                case 1:ianuarie+=unitPrice * quantity;
                    break;
                case 2:februarie+=unitPrice * quantity;
                    break;
                case 3:martie+=unitPrice * quantity;
                    break;
                case 4:aprilie+=unitPrice * quantity;
                    break;
                case 5:mai+=unitPrice * quantity;
                    break;
                case 6:iunie+=unitPrice * quantity;
                    break;
                case 7:iulie+=unitPrice * quantity;
                    break;
                case 8:august+=unitPrice * quantity;
                    break;
                case 9:septembrie+=unitPrice * quantity;
                    break;
                case 10:octombrie+=unitPrice * quantity;
                    break;
                case 11:noiembrie+=unitPrice * quantity;
                    break;
                case 12:decembrie+=unitPrice * quantity;
                    break;
            }
        }
        monthIn.add(ianuarie);
        monthIn.add(februarie);
        monthIn.add(martie);
        monthIn.add(aprilie);
        monthIn.add(mai);
        monthIn.add(iunie);
        monthIn.add(iulie);
        monthIn.add(august);
        monthIn.add(septembrie);
        monthIn.add(octombrie);
        monthIn.add(noiembrie);
        monthIn.add(decembrie);
        System.out.println(monthIn.toString());
        return  monthIn;
    }

}
