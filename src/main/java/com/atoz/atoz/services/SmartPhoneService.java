package com.atoz.atoz.services;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.dtos.builders.CustomerBuilder;
import com.atoz.atoz.dtos.builders.ElectronicsBuilder.SmartPhoneBuilder;
import com.atoz.atoz.dtos.builders.FashionBuilder.FashionBuilder;
import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.entities.product.Electronics.SmartPhone;
import com.atoz.atoz.entities.product.Fashion.FashionItem;
import com.atoz.atoz.repositories.SmartPhoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SmartPhoneService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartPhoneService.class);
    private final SmartPhoneRepository smartPhoneRepository;


    public SmartPhoneService(SmartPhoneRepository smartPhoneRepository) {
        this.smartPhoneRepository = smartPhoneRepository;
    }

    public List<SmartPhoneDTO> findSmartPhone() {
        List<SmartPhone> smartPhoneList = smartPhoneRepository.findAll();
        return smartPhoneList.stream()
                .map(SmartPhoneBuilder::toSmartPhoneDTO)
                .collect(Collectors.toList());
    }

    public SmartPhoneDTO findSmartPhoneById(int id){
        Optional<SmartPhone> smartPhone = smartPhoneRepository.findById(id);
        return SmartPhoneBuilder.toSmartPhoneDTO(smartPhone.get());
    }

    public int insert(SmartPhoneDTO smartPhoneDTO){
        SmartPhone smartPhone = SmartPhoneBuilder.toEntity(smartPhoneDTO);
        smartPhone = smartPhoneRepository.save(smartPhone);
        LOGGER.debug("smartphone with id {} was inserted in db", smartPhone.getId());
        return smartPhone.getId();
    }

    public int delete(int id){
        Optional<SmartPhone> optionalSmartPhone = smartPhoneRepository.findById(id);
        SmartPhone smartPhone = optionalSmartPhone.get();


        smartPhoneRepository.deleteById(id);
        return id;
    }


}
