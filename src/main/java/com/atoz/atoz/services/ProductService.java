package com.atoz.atoz.services;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.ProductDTO;
import com.atoz.atoz.dtos.builders.CustomerBuilder;
import com.atoz.atoz.dtos.builders.ElectronicsBuilder.SmartPhoneBuilder;
import com.atoz.atoz.dtos.builders.ProductBuilder;
import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.entities.product.Electronics.SmartPhone;
import com.atoz.atoz.entities.product.Product;
import com.atoz.atoz.repositories.ProductsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
    private final ProductsRepository productsRepository;

    public ProductService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }
    public List<ProductDTO> findProduct() {
        List<Product> productList = productsRepository.findAll();
        return productList.stream()
                .map(ProductBuilder::productDTO)
                .collect(Collectors.toList());
    }


    public List<ProductDTO> findByName(String productName) {
        List<Product> productList = productsRepository.findByNameContaining(productName);
        return productList.stream()
                .map(ProductBuilder::productDTO)
                .collect(Collectors.toList());
    }

    public ProductDTO findProductById(int id){
        Optional<Product> product= productsRepository.findById(id);
        return ProductBuilder.productDTO(product.get());
    }

    public int insert(ProductDTO productDTO){
        Product product = ProductBuilder.toEntity(productDTO);
        product = productsRepository.save(product);
        LOGGER.debug("smartphone with id {} was inserted in db", product.getId());
        return product.getId();
    }

    public void updateStock(int id, int stock){
        Optional<Product> productToUpdate = productsRepository.findById(id);
        productToUpdate.get().setStock(stock);
        productsRepository.save(productToUpdate.get());
    }
}
