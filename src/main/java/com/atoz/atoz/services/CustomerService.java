package com.atoz.atoz.services;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.builders.CustomerBuilder;
import com.atoz.atoz.dtos.builders.FashionBuilder.FashionBuilder;
import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.entities.product.Fashion.FashionItem;
import com.atoz.atoz.repositories.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    public List<CustomerDTO> findCustomer() {
        List<Customer> customerList = customerRepository.findAll();
        return customerList.stream()
                .map(CustomerBuilder::toCustomerDTO)
                .collect(Collectors.toList());
    }

    public CustomerDTO findCustomerById(UUID id){
        Optional<Customer> customer = customerRepository.findById(id);
        return CustomerBuilder.toCustomerDTO(customer.get());
    }

    public UUID insert(CustomerDTO customerDTO){
        Customer customer = CustomerBuilder.toEntity(customerDTO);
        customer = customerRepository.save(customer);
        LOGGER.debug("customer with id {} was inserted in db", customer.getId());
        return customer.getId();
    }

}
