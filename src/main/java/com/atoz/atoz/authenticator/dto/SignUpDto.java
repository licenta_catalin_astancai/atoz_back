package com.atoz.atoz.authenticator.dto;

import javax.validation.constraints.NotBlank;

public class SignUpDto {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @NotBlank
    private String nume;

    @NotBlank
    private String telefon;

    @NotBlank
    private String adresa;


    public SignUpDto(String username, String password, String name, String telefon, String adresa) {
        this.username = username;
        this.password = password;
        this.nume = name;
        this.telefon = telefon;
        this.adresa = adresa;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String name) {
        this.nume = name;
    }

    public String getTelfon() {
        return telefon;
    }

    public void setTelfon(String telfon) {
        this.telefon = telfon;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}
