package com.atoz.atoz.authenticator;

import com.atoz.atoz.authenticator.dto.LoginDTO;
import com.atoz.atoz.authenticator.dto.SignUpDto;
import com.atoz.atoz.authenticator.repo.UserRepository;
import com.atoz.atoz.authenticator.response.JwtResponse;
import com.atoz.atoz.authenticator.response.MessageResponse;
import com.atoz.atoz.authenticator.security.JwtUtils;
import com.atoz.atoz.authenticator.security.UserDetailsImpl;
import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AutentificatorController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {

        System.out.println(loginDTO.getUsername() + " "+ loginDTO.getPassword());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();


        if(loginDTO.getUsername().equals("admin")){
            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    "admin"));
        }


        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                "customer"));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpDto signUpDTO) {
        if (userRepository.existsByUsername(signUpDTO.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if(signUpDTO.getUsername().equals("admin")){
            Customer customer = new Customer(signUpDTO.getUsername(),encoder.encode(signUpDTO.getPassword()),"admin",signUpDTO.getNume(),signUpDTO.getTelfon(),signUpDTO.getAdresa());
            customerRepository.save(customer);
            return ResponseEntity.ok(new MessageResponse("Adimn registered successfully!"));
        }
        Customer customer = new Customer(signUpDTO.getUsername(),encoder.encode(signUpDTO.getPassword()),"customer",signUpDTO.getNume(),signUpDTO.getTelfon(),signUpDTO.getAdresa());
        customerRepository.save(customer);
        return ResponseEntity.ok(new MessageResponse("Customer registered successfully!"));
    }

}
