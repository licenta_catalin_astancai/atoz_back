package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.UsersDTO;
import com.atoz.atoz.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UsersDTO>> getUsers() {
        List<UsersDTO> dtos =  userService.finUsers();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UsersDTO> getUserById(@PathVariable("id") UUID id){
        UsersDTO dto= userService.findUserById(id);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

}
