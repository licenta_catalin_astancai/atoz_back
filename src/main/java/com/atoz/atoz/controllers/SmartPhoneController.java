package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.services.SmartPhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/smartPhone")
public class SmartPhoneController {

    private final SmartPhoneService smartPhoneService;

    @Autowired
    public SmartPhoneController(SmartPhoneService smartPhoneService) {
        this.smartPhoneService = smartPhoneService;
    }



    @GetMapping()
    public ResponseEntity<List<SmartPhoneDTO>> getSmartPhones() {
        List<SmartPhoneDTO> dtos =  smartPhoneService.findSmartPhone();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<SmartPhoneDTO> getSmartPhoneById(@PathVariable("id") int id){
        SmartPhoneDTO dto= smartPhoneService.findSmartPhoneById(id);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertCustomer(@RequestBody SmartPhoneDTO smartPhoneDTO) {
        int smartPhoneid = smartPhoneService.insert(smartPhoneDTO);
        return new ResponseEntity<>(smartPhoneid, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Integer>deleteSmartPhone(@PathVariable("id") int id)
    {
        int a= smartPhoneService.delete(id);

        return new ResponseEntity<>(a,HttpStatus.OK);
    }
}
