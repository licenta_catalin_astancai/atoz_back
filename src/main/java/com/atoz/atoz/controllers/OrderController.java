package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.Order.OrderDTO;
import com.atoz.atoz.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/order")
public class OrderController {

    private final OrderService orderService;
    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping()
    public ResponseEntity<List<OrderDTO>> getOrder() {
        List<OrderDTO> dtos =  orderService.findOrder();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/data")
    public ResponseEntity<List<Integer>> processOrderMonth() {
       List<Integer> dtos =  orderService.processOrderMonth();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertOrder(@RequestBody OrderDTO orderDTO) {
        int orderID = orderService.insert(orderDTO);
        return new ResponseEntity<>(orderID, HttpStatus.CREATED);
    }
}
