package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.services.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/customer")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    public ResponseEntity<List<CustomerDTO>> getCustomer() {
        List<CustomerDTO> dtos =  customerService.findCustomer();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<CustomerDTO> getCustomerById(@PathVariable("id") UUID id){
        CustomerDTO dto= customerService.findCustomerById(id);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCustomer(@RequestBody CustomerDTO customerDTO) {
        UUID customerID = customerService.insert(customerDTO);
        return new ResponseEntity<>(customerID, HttpStatus.CREATED);
    }

}
