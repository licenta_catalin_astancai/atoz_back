package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.ProductDTO;
import com.atoz.atoz.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/products")
public class ProductsController {

    private final ProductService productService;

    @Autowired
    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping()
    public ResponseEntity<List<ProductDTO>> getProducts(){
        List<ProductDTO> productDTOS = productService.findProduct();
        return new ResponseEntity<>(productDTOS, HttpStatus.OK);
    }


    @GetMapping(value = "/{name}")
    public ResponseEntity<List<ProductDTO>> getProductByName(@PathVariable("name") String prodName){
        List<ProductDTO> productDTOS= productService.findByName(prodName);
        return new ResponseEntity<>(productDTOS,HttpStatus.OK);
    }

    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable("id") int id){
        ProductDTO dto= productService.findProductById(id);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

}
