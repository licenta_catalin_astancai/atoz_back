package com.atoz.atoz.controllers;

import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.services.FashionItemService;
import com.atoz.atoz.services.SmartPhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/fashion")
public class FashionItemController {
    private final FashionItemService fashionItemService;
    @Autowired
    public FashionItemController(FashionItemService fashionItemService) {
        this.fashionItemService = fashionItemService;
    }
    @GetMapping()
    public ResponseEntity<List<FashionItemDTO>> getFashionItems() {
        List<FashionItemDTO> dtos =  fashionItemService.findFashionItem();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<FashionItemDTO> getFashionItemById(@PathVariable("id") int id){
        FashionItemDTO dto= fashionItemService.findFashionItemById(id);
        return new ResponseEntity<>(dto,HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertCustomer(@RequestBody FashionItemDTO fashionItemDTO) {
        int fashionItemId = fashionItemService.insert(fashionItemDTO);
        return new ResponseEntity<>(fashionItemId, HttpStatus.CREATED);
    }
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Integer>deleteFashionItem(@PathVariable("id") int id)
    {
        int fashionId= fashionItemService.delete(id);

        return new ResponseEntity<>(fashionId,HttpStatus.OK);
    }
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Integer>updateFashion(@PathVariable("id") int id,@RequestBody FashionItemDTO fashionItemDTO)
    {
        int fashionId= fashionItemService.update(id,fashionItemDTO);

        return new ResponseEntity<>(fashionId,HttpStatus.OK);
    }
}
