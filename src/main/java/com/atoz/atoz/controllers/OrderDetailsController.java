package com.atoz.atoz.controllers;


import com.atoz.atoz.dtos.Order.OrderDTO;
import com.atoz.atoz.dtos.Order.OrderDetailsDTO;
import com.atoz.atoz.dtos.Order.OrdersDetailsDTO;
import com.atoz.atoz.services.OrderDetailsService;
import com.atoz.atoz.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/orderdetails")
public class OrderDetailsController {

    private final OrderService orderService;
    private final OrderDetailsService orderDetailsService;
    @Autowired
    public OrderDetailsController(OrderService orderService,OrderDetailsService orderDetailsService) {
        this.orderService = orderService;
        this.orderDetailsService=orderDetailsService;
    }

    @GetMapping()
    public ResponseEntity<List<OrderDetailsDTO>> getOrderDetail() {
        List<OrderDetailsDTO> dtos =  orderDetailsService.findOrdersDetails();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value="/turnover")
    public ResponseEntity<List<Float>> getOrderDetailBestSeller() {
        List<Float>dto= orderDetailsService.turnOverPerMonth();
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Integer> insertOrderDetail(@RequestBody OrdersDetailsDTO ordersDetailsDTO) {

        int id =orderDetailsService.insert(ordersDetailsDTO);
        return new ResponseEntity<>(id,HttpStatus.CREATED);
    }
}

