package com.atoz.atoz.repositories;

import com.atoz.atoz.entities.product.Fashion.FashionItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FashionItemRepository extends JpaRepository<FashionItem, Integer> {
}
