package com.atoz.atoz.repositories;

import com.atoz.atoz.entities.Order;
import com.atoz.atoz.entities.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails,Integer> {
}
