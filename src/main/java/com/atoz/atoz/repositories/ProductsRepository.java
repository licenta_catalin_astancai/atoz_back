package com.atoz.atoz.repositories;

import com.atoz.atoz.entities.Users;
import com.atoz.atoz.entities.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface ProductsRepository extends JpaRepository<Product, Integer> {
    List<Product> findByNameContaining(String infix);
}
