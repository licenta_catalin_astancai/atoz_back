package com.atoz.atoz.repositories;


import com.atoz.atoz.entities.product.Electronics.SmartPhone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SmartPhoneRepository  extends JpaRepository<SmartPhone, Integer> {

}
