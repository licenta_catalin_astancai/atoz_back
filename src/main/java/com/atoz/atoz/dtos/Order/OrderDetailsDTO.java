package com.atoz.atoz.dtos.Order;

import com.atoz.atoz.entities.Order;
import com.atoz.atoz.entities.product.Product;

public class OrderDetailsDTO {

    private int id;
    private Order order;
    private Product product;
    private float unitPrice;
    private int quantity;

    public OrderDetailsDTO(int id, Order order, Product product, float unitPrice, int quantity) {
        this.id = id;
        this.order = order;
        this.product = product;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
