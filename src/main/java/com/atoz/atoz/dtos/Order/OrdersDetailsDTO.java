package com.atoz.atoz.dtos.Order;

import com.atoz.atoz.entities.Customer;
import com.atoz.atoz.entities.Order;
import com.atoz.atoz.entities.product.Product;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class OrdersDetailsDTO {
    private int id;
    private Customer customerID;
    private Date date;
    private List<Product> productList = new ArrayList<Product>();
    private List<Float> unitPriceList = new ArrayList<Float>();
    private List<Integer> quantity= new ArrayList<Integer>();
/*
    public OrdersDetailsDTO(int id, Order order, List<Product> productList, List<Float> unitPriceList, List<Integer> quantity) {
        this.id = id;
        this.order = order;
        this.productList = productList;
        this.unitPriceList = unitPriceList;
        this.quantity = quantity;
    }
*/

    public OrdersDetailsDTO(int id, Customer customerID, Date date, List<Product> productList, List<Float> unitPriceList, List<Integer> quantity) {
        this.id = id;
        this.customerID = customerID;
        this.date = date;
        this.productList = productList;
        this.unitPriceList = unitPriceList;
        this.quantity = quantity;
    }

    public OrdersDetailsDTO() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
/*
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
*/

    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public List<Float> getUnitPriceList() {
        return unitPriceList;
    }

    public void setUnitPriceList(List<Float> unitPriceList) {
        this.unitPriceList = unitPriceList;
    }

    public List<Integer> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<Integer> quantity) {
        this.quantity = quantity;
    }
}
