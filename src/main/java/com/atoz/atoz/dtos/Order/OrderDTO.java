package com.atoz.atoz.dtos.Order;

import com.atoz.atoz.entities.Customer;

import java.sql.Date;

public class OrderDTO {
    private int id;
    private Customer customer;
    private Date orderDate;

    public OrderDTO(int id, Customer customer, Date orderDate) {
        this.id = id;
        this.customer = customer;
        this.orderDate = orderDate;
    }

    public OrderDTO(Customer customer, Date orderDate) {
        this.customer = customer;
        this.orderDate = orderDate;
    }

    public OrderDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
}
