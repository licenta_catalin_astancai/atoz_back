package com.atoz.atoz.dtos.FashionDTOS;

import com.atoz.atoz.dtos.ProductDTO;


public class FashionItemDTO extends ProductDTO {

    private int id;
    private String gender;
    private String type;
    private String brand;
    private String size;
    private String color;

    public FashionItemDTO(String name, float price, String productImage,int stock,String category, int id,String gender ,String type, String brand, String size, String color) {
        super(name, price, productImage,stock,category);
        this.id = id;
        this.gender=gender;
        this.type = type;
        this.brand = brand;
        this.size = size;
        this.color = color;
    }

    @Override
    public int getId() {
        return id;
    }


    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
