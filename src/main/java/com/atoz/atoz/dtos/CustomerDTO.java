package com.atoz.atoz.dtos;

import java.util.UUID;

public class CustomerDTO extends  UsersDTO{

    private UUID id;
    private String nume;
    private String telefon;
    private String adresa;

    public CustomerDTO(UUID id, String username, String password,String role, String nume, String telefon, String adresa) {
        super(username, password,role);
        this.id = id;
        this.nume = nume;
        this.telefon = telefon;
        this.adresa = adresa;
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}
