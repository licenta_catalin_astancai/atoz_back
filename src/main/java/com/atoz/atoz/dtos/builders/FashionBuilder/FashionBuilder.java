package com.atoz.atoz.dtos.builders.FashionBuilder;

import com.atoz.atoz.dtos.FashionDTOS.FashionItemDTO;
import com.atoz.atoz.entities.product.Fashion.FashionItem;

public class FashionBuilder {

    public FashionBuilder() {
    }

    public static FashionItemDTO toFashionItemDTO (FashionItem fashionItem){
        return new FashionItemDTO(fashionItem.getName(), fashionItem.getPrice(), fashionItem.getProductImage(), fashionItem.getStock(), fashionItem.getCategory(), fashionItem.getId(),fashionItem.getGender(),fashionItem.getType(),fashionItem.getBrand(),fashionItem.getSize(),fashionItem.getColor());
    }

    public static FashionItem toEntity(FashionItemDTO fashionItemDTO){
        return new FashionItem(fashionItemDTO.getName(),fashionItemDTO.getPrice(),fashionItemDTO.getProductImage(), fashionItemDTO.getStock(), fashionItemDTO.getCategory(), fashionItemDTO.getGender(),fashionItemDTO.getType(),fashionItemDTO.getBrand(),fashionItemDTO.getSize(),fashionItemDTO.getColor());
    }
}
