package com.atoz.atoz.dtos.builders;

import com.atoz.atoz.dtos.CustomerDTO;
import com.atoz.atoz.entities.Customer;

public class CustomerBuilder {

    private CustomerBuilder(){

    }

    public static CustomerDTO toCustomerDTO(Customer customer){
        return new CustomerDTO(customer.getId(), customer.getUsername(), customer.getPassword(), customer.getRole(), customer.getNume(), customer.getTelefon(), customer.getAdresa());
    }

    public static Customer toEntity(CustomerDTO customerDTO){
        return new Customer(customerDTO.getUsername(), customerDTO.getPassword(), customerDTO.getRole(), customerDTO.getNume(), customerDTO.getTelefon(), customerDTO.getAdresa());
    }
}
