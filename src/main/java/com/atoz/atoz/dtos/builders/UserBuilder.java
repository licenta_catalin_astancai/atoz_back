package com.atoz.atoz.dtos.builders;

import com.atoz.atoz.dtos.UsersDTO;
import com.atoz.atoz.entities.Users;

public class UserBuilder {
    public UserBuilder() {
    }


    public static UsersDTO usersDTO(Users u ){
        return new UsersDTO(u.getId(),u.getUsername(),u.getPassword(),u.getRole());
    }
    public static Users toEntity (UsersDTO usersDTO){
        return new Users(usersDTO.getId(), usersDTO.getUsername(), usersDTO.getPassword(), usersDTO.getRole());
    }
}
