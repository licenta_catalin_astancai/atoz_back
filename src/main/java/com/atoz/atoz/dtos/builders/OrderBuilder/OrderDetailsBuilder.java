package com.atoz.atoz.dtos.builders.OrderBuilder;

import com.atoz.atoz.dtos.Order.OrderDetailsDTO;
import com.atoz.atoz.entities.OrderDetails;

public class OrderDetailsBuilder {

    public OrderDetailsBuilder() {
    }

    public static OrderDetailsDTO toOrderDetailsDTO (OrderDetails orderDetails){
        return  new OrderDetailsDTO(orderDetails.getId(),orderDetails.getOrder(),orderDetails.getProduct(),orderDetails.getUnitPrice(),orderDetails.getQuantity());
    }

    public static  OrderDetails toEntity( OrderDetailsDTO orderDetailsDTO){
        return  new OrderDetails(orderDetailsDTO.getOrder(),orderDetailsDTO.getProduct(),orderDetailsDTO.getUnitPrice(),orderDetailsDTO.getQuantity());
    }
}
