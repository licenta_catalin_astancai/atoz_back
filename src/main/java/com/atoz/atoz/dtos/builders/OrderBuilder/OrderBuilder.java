package com.atoz.atoz.dtos.builders.OrderBuilder;

import com.atoz.atoz.dtos.Order.OrderDTO;
import com.atoz.atoz.entities.Order;


public class OrderBuilder {

    public OrderBuilder() {
    }

    public static OrderDTO toOrderDTO(Order order) {
        return new OrderDTO(order.getId(), order.getCustomer(), order.getOrderDate());
    }




    public static Order toEntity (OrderDTO orderDTO){
        return new Order(orderDTO.getCustomer(),orderDTO.getOrderDate());
    }
}