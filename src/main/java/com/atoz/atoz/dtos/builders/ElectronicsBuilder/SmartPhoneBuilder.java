package com.atoz.atoz.dtos.builders.ElectronicsBuilder;

import com.atoz.atoz.dtos.ElectronicsDTOS.SmartPhoneDTO;
import com.atoz.atoz.entities.product.Electronics.SmartPhone;

public class SmartPhoneBuilder {
    public SmartPhoneBuilder() {
    }

    public  static SmartPhoneDTO toSmartPhoneDTO(SmartPhone smartPhone){
        return new SmartPhoneDTO(smartPhone.getName(),smartPhone.getPrice(),smartPhone.getProductImage(), smartPhone.getStock(), smartPhone.getCategory(), smartPhone.getId(), smartPhone.getBrand(), smartPhone.getModel(),smartPhone.getDispsize(),smartPhone.getResolution(),smartPhone.getMaincamera(),smartPhone.getFrontcamera(),smartPhone.getCpu(),smartPhone.getMemory(),smartPhone.getBattery(),smartPhone.getDescription());
    }

    public  static SmartPhone toEntity( SmartPhoneDTO smartPhoneDTO){

        return  new SmartPhone(smartPhoneDTO.getName(),smartPhoneDTO.getPrice(),smartPhoneDTO.getProductImage(), smartPhoneDTO.getStock(), smartPhoneDTO.getCategory(), smartPhoneDTO.getBrand(),smartPhoneDTO.getModel(),smartPhoneDTO.getDispsize(),smartPhoneDTO.getResolution(),smartPhoneDTO.getMaincamera(),smartPhoneDTO.getFrontcamera(),smartPhoneDTO.getCpu(),smartPhoneDTO.getMemory(),smartPhoneDTO.getBattery(),smartPhoneDTO.getDescription());
    }

}
