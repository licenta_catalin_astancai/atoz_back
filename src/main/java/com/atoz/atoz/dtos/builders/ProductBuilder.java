package com.atoz.atoz.dtos.builders;

import com.atoz.atoz.dtos.ProductDTO;
import com.atoz.atoz.entities.product.Product;

public class ProductBuilder {
    public ProductBuilder() {
    }
    public static ProductDTO productDTO(Product product){
        return new ProductDTO(product.getId(),product.getName(),product.getPrice(), product.getProductImage(),product.getStock(), product.getCategory());
    }
    public  static Product toEntity(ProductDTO productDTO)
    {
        return new Product(productDTO.getId(), productDTO.getName(), productDTO.getPrice(), productDTO.getProductImage(), productDTO.getStock(), productDTO.getCategory());
    }

}
