package com.atoz.atoz.dtos.ElectronicsDTOS;

import com.atoz.atoz.dtos.ProductDTO;

public class SmartPhoneDTO extends ProductDTO {

    private int id;
    private String brand;
    private String model;
    private String dispsize;
    private String resolution;
    private String maincamera;
    private String frontcamera;
    private String cpu;
    private String memory;
    private String battery;
    private String description;

    public SmartPhoneDTO(String name, float price, String productImage,int stock,String category, int id,String brand, String model, String dispsize, String resolution, String maincamera, String frontcamera, String cpu, String memory, String battery, String description) {
        super(name, price, productImage,stock,category);
        this.id = id;
        this.brand=brand;
        this.model = model;
        this.dispsize = dispsize;
        this.resolution = resolution;
        this.maincamera = maincamera;
        this.frontcamera = frontcamera;
        this.cpu = cpu;
        this.memory = memory;
        this.battery = battery;
        this.description = description;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDispsize() {
        return dispsize;
    }

    public void setDispsize(String dispsize) {
        this.dispsize = dispsize;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getMaincamera() {
        return maincamera;
    }

    public void setMaincamera(String maincamera) {
        this.maincamera = maincamera;
    }

    public String getFrontcamera() {
        return frontcamera;
    }

    public void setFrontcamera(String frontcamera) {
        this.frontcamera = frontcamera;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
