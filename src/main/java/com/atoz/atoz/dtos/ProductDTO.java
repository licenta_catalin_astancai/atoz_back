package com.atoz.atoz.dtos;

public class ProductDTO {

    private int id;
    private String name;
    private float price;
    private String productImage;
    private int stock;
    private String category;

    public ProductDTO(String name, float price, String productImage,int stock,String category) {
        this.name = name;
        this.price = price;
        this.productImage = productImage;
        this.stock=stock;
        this.category=category;
    }

    public ProductDTO(int id, String name, float price, String productImage,int stock,String category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.productImage = productImage;
        this.stock=stock;
        this.category=category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}
