package com.atoz.atoz.dtos;



import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UsersDTO {
    private  UUID id;
    private String username;
    private String password;
    private String role;
    public UsersDTO(){

    }
    public UsersDTO(String username, String password,String role) {
        this.username = username;
        this.password = password;
        this.role=role;
    }
    public UsersDTO(UUID id,String username, String password,String role) {
        this.id=id;
        this.username = username;
        this.password = password;
        this.role=role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}